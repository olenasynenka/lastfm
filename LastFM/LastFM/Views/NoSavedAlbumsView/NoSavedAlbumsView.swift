//
//  NoSavedAlbumsView.swift
//  LastFM
//
//  Created by Olena Synenka on 2/14/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

final class NoSavedAlbumsView: UIView {
    
    // MARK: - LifeCycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        prepareView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        prepareView()
    }
    
    // MARK: - Private
    
    fileprivate func prepareView() {
        let nameForXib = String(describing: NoSavedAlbumsView.self)
        let nibs = Bundle.main.loadNibNamed(nameForXib, owner: self, options: nil)
        if let view = nibs?.first as? UIView {
            view.backgroundColor = UIColor.clear
            view.translatesAutoresizingMaskIntoConstraints = false
            addSubviewWithConstraints(view)
        }
    }
}
