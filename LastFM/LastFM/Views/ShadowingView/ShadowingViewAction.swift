import UIKit

class ShadowingViewAction: NSObject, CAAction {

    var pendingAnimation: CABasicAnimation?
    var priorPath: CGPath?

    // MARK: - CAAction
    func run(forKey event: String, object anObject: Any, arguments dict: [AnyHashable: Any]?) {
        guard let layer = anObject as? CALayer, let animation = self.pendingAnimation else {
            return
        }

        animation.fromValue = self.priorPath
        animation.toValue = layer.shadowPath
        layer.add(animation, forKey: "shadowPath")
    }
}
