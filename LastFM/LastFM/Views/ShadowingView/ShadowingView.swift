import UIKit

final class ShadowingView: UIView {

    var shadowColor: UIColor = UIColor.black.withAlphaComponent(0.3)
    var shadowOffset: CGSize = CGSize(width: -1, height: 1)

    // MARK: - Life Cycle

    override func layoutSubviews() {
        super.layoutSubviews()

        addShadowLayer()
        updateShadowLayerPath()
    }

    override func action(for layer: CALayer, forKey event: String) -> CAAction? {
        guard event == "shadowPath" else {
            return super.action(for: layer, forKey: event)
        }

        guard let priorPath = layer.shadowPath else {
            return super.action(for: layer, forKey: event)
        }

        guard let sizeAnimation = layer.animation(forKey: "bounds.size") as? CABasicAnimation else {
            return super.action(for: layer, forKey: event)
        }

        let animation = sizeAnimation.copy() as? CABasicAnimation
        animation?.keyPath = "shadowPath"
        let action = ShadowingViewAction()
        action.priorPath = priorPath
        guard let anim = animation else {
            return nil
        }
        action.pendingAnimation = anim
        return action
    }

    // MARK: - Private

    private func addShadowLayer() {
        clipsToBounds = false

        layer.shadowColor = shadowColor.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = shadowOffset
        layer.masksToBounds = false
    }

    private func updateShadowLayerPath() {
        layoutIfNeeded()
        let path = UIBezierPath(roundedRect: bounds.insetBy(dx: -0.5, dy: -0.5), cornerRadius: 4).cgPath
        layer.shadowPath = path
    }
}
