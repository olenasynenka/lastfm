//
//  AlbumDetailsParseHelperObject.swift
//  LastFM
//
//  Created by Olena Synenka on 2/12/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

struct AlbumDetailsParseHelperObject: Decodable {
    
    var album: AlbumDetailsParseObject
}

struct AlbumDetailsParseObject: Decodable {
    
    var tracks: TracksArrayParseObject
}

struct TracksArrayParseObject: Decodable {
    var track: [TrackParseObject]
}

struct TrackParseObject: Decodable {
    
    var name: String
    var duration: String
    var attribute: TrackAttributeParseObject
    
    private enum CodingKeys: String, CodingKey {
        case attribute = "@attr"
        case duration
        case name
    }
}

struct TrackAttributeParseObject: Decodable {
    var number: String
    
    private enum CodingKeys: String, CodingKey {
        case number = "rank"
    }
}
