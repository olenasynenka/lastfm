//
//  ArtistAlbumsParseHelperObject.swift
//  LastFM
//
//  Created by Olena Synenka on 2/11/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import Foundation

struct ArtistAlbumsParseHelperObject: Decodable {
    var topAlbums: ArtistAlbumsObject
    
    private enum CodingKeys: String, CodingKey {
        case topAlbums = "topalbums"
    }
}

struct ArtistAlbumsObject: Decodable {
    var albums: [AlbumParseObject]
    var albumAttributes: AlbumAttributes
    
    private enum CodingKeys: String, CodingKey {
        case albumAttributes = "@attr"
        case albums = "album"
    }
}

struct AlbumParseObject: Decodable {
    
    private (set) var name: String
    private (set) var mbid: String?
    private (set) var image: [ImageParseHelperObject]
}

struct AlbumAttributes: Decodable {
    var total: String
}
