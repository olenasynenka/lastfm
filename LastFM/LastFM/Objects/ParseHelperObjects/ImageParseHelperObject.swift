//
//  ImageParseHelperObject.swift
//  LastFM
//
//  Created by Olena Synenka on 2/10/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

enum ImageSize: String, Decodable {
    case small
    case medium
    case large
    case extralarge
    case mega
}

struct ImageParseHelperObject: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case urlString = "#text"
        case size
    }
            
    let urlString: String
    let size: ImageSize
}
