//
//  ArtistsSearchParseHelperObject.swift
//  LastFM
//
//  Created by Olena Synenka on 2/10/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

struct ArtistsSearchParseHelperObject: Decodable {
    
    let results: SearchResult?
}

struct SearchResult: Decodable {
    let artistmatches: ArtistMatches
    let totalResults: String
    
    private enum CodingKeys: String, CodingKey {
        case totalResults = "opensearch:totalResults"
        case artistmatches
    }
}

struct ArtistMatches: Decodable {
    let artist: [ArtistParseObject]
}

struct ArtistParseObject: Decodable {
    private (set) var name: String
    private (set) var image: [ImageParseHelperObject]
    private (set) var mbid: String
}
