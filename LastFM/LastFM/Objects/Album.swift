//
//  Album.swift
//  LastFM
//
//  Created by Olena Synenka on 2/10/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

final class Album {
    
    private (set) var name: String
    private (set) var artist: Artist?
    private (set) var id: String
    private (set) var isLiked: Bool = false
    
    private (set) var mediumImageURL: String
    private (set) var largeImageURL: String
    
    private (set) var mediumImage: UIImage?
    private (set) var largeImage: UIImage?
    
    private (set) var tracks: [Track]?
    
    // MARK: - Life Cycle
    
    init(albumResponse: AlbumResponse, artist: Artist) {
        
        self.name = albumResponse.name
        self.id = albumResponse.id
        self.mediumImageURL = albumResponse.mediumImageURL
        self.largeImageURL = albumResponse.largeImageURL
        
        self.artist = artist
    }
    
    init(realmAlbum: RealmAlbum) {
        
        self.name = realmAlbum.name
        self.id = realmAlbum.id
        self.mediumImageURL = realmAlbum.mediumImageURL
        self.largeImageURL = realmAlbum.largeImageURL
        self.isLiked = true
        
        if let realmArtist = realmAlbum.artist {
            let artist = Artist(realmArtist: realmArtist)
            self.artist = artist
        }
        
        tracks = []
        realmAlbum.tracks.forEach({
            let track = Track(realmTrack: $0)
            tracks?.append(track)
        })
    }
    
    // MARK: - Public
    
    func addLargeImage(_ image: UIImage) {
        self.largeImage = image
    }
    
    func addMediumImage(_ image: UIImage) {
        self.mediumImage = image
    }
    
    func setupWithTracks(_ tracks: [Track]) {
        self.tracks = tracks
    }
    
    func setupWithArtist(_ artist: Artist) {
        self.artist = artist
    }
    
    func like() {
        isLiked = true
    }
    
    func dislike() {
        isLiked = false
    }
}
