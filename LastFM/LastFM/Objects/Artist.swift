//
//  Artist.swift
//  LastFM
//
//  Created by Olena Synenka on 2/13/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

final class Artist {
    
    private (set) var name: String
    private (set) var id: String

    private (set) var imageURL: String
    private (set) var image: UIImage?

    // MARK: - Life Cycle
    
    init(name: String, id: String, imageURL: String) {
        self.name = name
        self.id = id
        self.imageURL = imageURL
    }
    
    init(realmArtist: RealmArtist) {
        self.name = realmArtist.name
        self.id = realmArtist.id
        self.imageURL = realmArtist.imageURL
    }
    
    // MARK: - Public
    
    func setArtistPhotoImage(_ image: UIImage) {
        self.image = image
    }
}
