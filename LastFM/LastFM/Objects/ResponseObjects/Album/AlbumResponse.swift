//
//  AlbumResponse.swift
//  LastFM
//
//  Created by Olena Synenka on 2/11/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

struct AlbumResponse: Decodable {
    
    private (set) var name: String
    private (set) var id: String
    private (set) var mediumImageURL: String
    private (set) var largeImageURL: String
}
