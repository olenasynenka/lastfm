//
//  ArtistAlbumsResponse.swift
//  LastFM
//
//  Created by Olena Synenka on 2/11/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

struct ArtistAlbumsResponse {
    var totalResults: Int
    var albums: [AlbumResponse]
    
    init(parseHelperObject: ArtistAlbumsParseHelperObject) {
        totalResults = Int(parseHelperObject.topAlbums.albumAttributes.total) ?? 0
        
        albums = []
        let albumParseObjects = parseHelperObject.topAlbums.albums
        albumParseObjects.forEach { albumParseObject in
            if let albumId = albumParseObject.mbid,
                let mediumImageURL = albumParseObject.image.first(where: { $0.size == .large })?.urlString,
                let largeImageURL = albumParseObject.image.first(where: { $0.size == .extralarge })?.urlString {
                let album = AlbumResponse(name: albumParseObject.name,
                                          id: albumId,
                                          mediumImageURL: mediumImageURL,
                                          largeImageURL: largeImageURL)
                albums.append(album)
            }
        }
    }
}
