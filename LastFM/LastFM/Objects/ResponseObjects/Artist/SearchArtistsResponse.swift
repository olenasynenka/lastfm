//
//  SearchArtistResponse.swift
//  LastFM
//
//  Created by Olena Synenka on 2/10/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import Foundation

struct SearchArtistsResponse {
    var artistResponses: [ArtistResponse]
    var totalResults: Int
    
    init(parseHelperObject: ArtistsSearchParseHelperObject) {
        let totalResultsString = parseHelperObject.results?.totalResults ?? ""
        totalResults = Int(totalResultsString) ?? 0
        
        artistResponses = []
        let artistParseObjects = parseHelperObject.results?.artistmatches.artist
        artistParseObjects?.forEach { artistParseObject in
            if let imageURL = artistParseObject.image.first(where: { $0.size == .small })?.urlString {
                let artist = ArtistResponse(id: artistParseObject.mbid, name: artistParseObject.name, imageURL: imageURL)
                artistResponses.append(artist)
            }
        }
    }
}
