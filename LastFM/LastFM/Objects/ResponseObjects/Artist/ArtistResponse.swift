//
//  ArtistResponse.swift
//  LastFM
//
//  Created by Olena Synenka on 2/10/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

struct ArtistResponse {
    
    private (set) var id: String
    private (set) var name: String
    private (set) var imageURL: String
}
