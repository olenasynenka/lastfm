//
//  TrackResponse.swift
//  LastFM
//
//  Created by Olena Synenka on 2/12/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

struct TrackResponse {
    
    var name: String
    var duration: Int
    var number: Int
}
