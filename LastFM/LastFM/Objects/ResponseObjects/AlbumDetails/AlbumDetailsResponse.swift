//
//  AlbumDetailsResponse.swift
//  LastFM
//
//  Created by Olena Synenka on 2/12/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

struct AlbumDetailsResponse {
    
    var tracks: [TrackResponse]
    
    // MARK: - Life Cycle
    
    init(parseHelperObject: AlbumDetailsParseHelperObject) {        
        self.tracks = []
        parseHelperObject.album.tracks.track.forEach({
            if let duration = Int($0.duration),
            let number = Int($0.attribute.number) {
                let track = TrackResponse(name: $0.name,
                                          duration: duration,
                                          number: number)
                self.tracks.append(track)
            }
        })
    }
}
