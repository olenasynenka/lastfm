//
//  Track.swift
//  LastFM
//
//  Created by Olena Synenka on 2/12/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

final class Track {
    
    private (set) var name: String
    private (set) var number: Int
    private (set) var duration: Int
    
    // MARK: - Life Cycle
    
    init(trackResponse: TrackResponse) {
        
        self.name = trackResponse.name
        self.number = trackResponse.number
        self.duration = trackResponse.duration
    }
    
    init(realmTrack: RealmTrack) {
        
        self.name = realmTrack.name
        self.number = realmTrack.number
        self.duration = realmTrack.duration
    }
}
