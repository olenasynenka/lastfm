//
//  StorageService.swift
//  LastFM
//
//  Created by Olena Synenka on 2/13/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import Foundation
import RealmSwift

final class StorageService {
    
    class func saveAlbum(_ album: Album) {
        let realmAlbum = RealmAlbum(album: album)
        save(object: realmAlbum)
        
        if let artistImage = album.artist?.image,
            let artistId = album.artist?.id {
            ImageSaver.saveImageWithName(artistImage, name: artistId)
        }
        
        if let albumMediumImage = album.mediumImage {
            ImageSaver.saveImageWithName(albumMediumImage, name: album.id + ImageSize.medium.rawValue)
        }
        
        if let albumLargeImage = album.largeImage {
            ImageSaver.saveImageWithName(albumLargeImage, name:  album.id + ImageSize.large.rawValue)
        }
    }
    
    class func enquireAlbums() -> [Album] {
        var albums: [Album] = []
        
        if let realm = initialRealm() {
            let realmObjects = realm.objects( RealmAlbum.self)
            realmObjects.forEach({
                let album = Album(realmAlbum: $0)
                fetchImagesForAlbum(album)
                albums.append(album)
            })
        }

        return albums
    }
    
    class func enquireAlbumsForArtist(_ artistId: String) -> [Album] {
        var albums: [Album] = []
        
        if let realm = initialRealm() {
            let realmAlbums = realm.objects(RealmAlbum.self).filter("artist.id = '\(artistId)'")
            realmAlbums.forEach({
                let album = Album(realmAlbum: $0)
                fetchImagesForAlbum(album)
                albums.append(album)
            })
        }
        
        return albums
    }
    
    class func removeAlbum(_ album: Album) {
        if let realm = initialRealm() {
            if let object = realm.object(ofType: RealmAlbum.self, forPrimaryKey: album.id) {
                remove(object: object)
                removeImagesForAlbum(album)
            }
        }
    }
    
    // MARK: - Private
    
    private class func fetchImagesForAlbum(_ album: Album) {
        if let artist = album.artist,
            let artistPhotoImage = ImageSaver.fetchImageWithName(artist.id) {
            album.artist?.setArtistPhotoImage(artistPhotoImage)
        }
        
        if let albumMediumImage = ImageSaver.fetchImageWithName(album.id + ImageSize.medium.rawValue) {
            album.addMediumImage(albumMediumImage)
        }
        
        if let albumLargeImage = ImageSaver.fetchImageWithName(album.id + ImageSize.large.rawValue) {
            album.addLargeImage(albumLargeImage)
        }
    }
    
    private class func removeImagesForAlbum(_ album: Album) {
        if album.artist?.image != nil,
            let artistId = album.artist?.id {
            ImageSaver.removeImageWithName(name: artistId)
        }
        
        if album.mediumImage != nil {
            ImageSaver.removeImageWithName(name: album.id + ImageSize.medium.rawValue)
        }
        
        if album.largeImage != nil {
            ImageSaver.removeImageWithName(name: album.id + ImageSize.large.rawValue)
        }
    }
    
    private class func initialRealm() -> Realm? {
        do {
            let realm = try Realm()
            return realm
        } catch {
            assertionFailure("realm is not initialised")
        }
        return nil
    }
    
    private class func save(object: Object) {
        if let realm = initialRealm() {
            do {
                try realm.write {
                    realm.add(object, update: true)
                }
            } catch {
                assertionFailure("realm filed to save")
            }
        }
    }
    
    private class func remove(object: Object) {
        if let realm = initialRealm() {
            do {
                try realm.write {
                    realm.delete(object)
                }
            } catch {
                assertionFailure("realm filed to remove")
            }
        }
    }
}
