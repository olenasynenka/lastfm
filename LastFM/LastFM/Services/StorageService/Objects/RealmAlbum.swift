//
//  RealmAlbum.swift
//  LastFM
//
//  Created by Olena Synenka on 2/13/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import RealmSwift

final class RealmAlbum: Object {
    
    @objc dynamic var id = ""
    @objc dynamic var name = ""
    @objc dynamic var artist: RealmArtist?
    @objc dynamic var mediumImageURL = ""
    @objc dynamic var largeImageURL = ""
    @objc dynamic var isLiked = false
    
    var tracks = List<RealmTrack>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // MARK: - Life Cycle
    
    convenience init(album: Album) {
        self.init()
        
        id = album.id
        name = album.name
        mediumImageURL = album.mediumImageURL
        largeImageURL = album.largeImageURL
        isLiked = album.isLiked
        
        if let artist = album.artist {
            let realmArtist = RealmArtist(artist: artist)
            self.artist = realmArtist
        }
        
        album.tracks?.forEach({
            let realmTrack = RealmTrack(track: $0)
            tracks.append(realmTrack)
        })
    }
}
