//
//  RealmTrack.swift
//  LastFM
//
//  Created by Olena Synenka on 2/13/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import RealmSwift

final class RealmTrack: Object {
    
    @objc dynamic var name = ""
    @objc dynamic var number = 0
    @objc dynamic var duration = 0
    
    convenience init(track: Track) {
        self.init()
        
        self.name = track.name
        self.number = track.number
        self.duration = track.duration
    }
}
