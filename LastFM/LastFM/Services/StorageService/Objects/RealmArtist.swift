//
//  RealmArtist.swift
//  LastFM
//
//  Created by Olena Synenka on 2/13/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import RealmSwift

class RealmArtist: Object {
    
    @objc dynamic var name = ""
    @objc dynamic var id = ""
    @objc dynamic var imageURL = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(artist: Artist) {
        self.init()
        
        id = artist.id
        name = artist.name
        imageURL = artist.imageURL
    }
}
