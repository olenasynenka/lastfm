//
//  APIService.swift
//  LastFM
//
//  Created by Olena Synenka on 2/10/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

final class APIService {
    
    private enum Constants {
        static let domain = "http://ws.audioscrobbler.com/2.0/"
        static let APIKey = "154bf944d09aa9a039a7e812eb679436"
    }
    
    static let shared = APIService()
    private let imageCache = AutoPurgingImageCache()

    // MARK: - Public
    
    func fetchImageFor(urlString: String, completion: @escaping ((UIImage?, Error?) -> ())) {
        if let image = imageCache.image(withIdentifier: urlString) {
            completion(image, nil)
            return
        }
        
        if let url = URL(string: urlString) {
            Alamofire.request(url).responseImage { (response) in
                let image = response.result.value
                if let image = image {
                    self.imageCache.add(image, withIdentifier: urlString)
                }
                
                completion(image, response.error)
            }
        }
    }
    
    func fetchArtistForSearchQuery(_ query: String, page: Int, completion: @escaping ((SearchArtistsResponse?, _ error: Error?) -> ())) {
        let query = query.replacingOccurrences(of: " ", with: "+")
        let requestURL = "\(Constants.domain)?page=\(page)&method=artist.search&artist=\(query)&api_key=\(Constants.APIKey)&format=json"
        
        Alamofire.request(requestURL).validate().response { (responseData) in
            if let error = responseData.error {
                completion(nil, error)
            } else if let data = responseData.data {
                do {
                    let decoder = JSONDecoder()
                    let searchResponseParseHelper = try decoder.decode(ArtistsSearchParseHelperObject.self, from: data)
                    let searchResponse = SearchArtistsResponse(parseHelperObject: searchResponseParseHelper)
                    
                    completion(searchResponse, responseData.error)
                } catch {
                    completion(nil, error)
                }
            }
        }
    }
    
    func fetchAlbumsForArtist(_ artistId: String, page: Int, completion: @escaping ((ArtistAlbumsResponse?, Error?) -> ())) {
        let requestURL = "\(Constants.domain)?page=\(page)&limit=20&method=artist.gettopalbums&mbid=\(artistId)&api_key=\(Constants.APIKey)&format=json"
        
        Alamofire.request(requestURL).validate().response { (responseData) in
            if let error = responseData.error {
                completion(nil, error)
            } else if let data = responseData.data {
                do {
                    let decoder = JSONDecoder()
                    let responseParseHelper = try decoder.decode(ArtistAlbumsParseHelperObject.self, from: data)
                    let albumsResponse = ArtistAlbumsResponse(parseHelperObject: responseParseHelper)
                    
                    completion(albumsResponse, responseData.error)
                } catch {
                    completion(nil, error)
                }
            }
        }
    }
    
    func fetchAlbumDetails(_ albumId: String, completion: @escaping ((AlbumDetailsResponse?, Error?) -> ())) {
        let requestURL = "\(Constants.domain)?&method=album.getinfo&mbid=\(albumId)&api_key=\(Constants.APIKey)&format=json"
        
        Alamofire.request(requestURL).validate().response { (responseData) in
            if let error = responseData.error {
                completion(nil, error)
            } else if let data = responseData.data {
                do {
                    let decoder = JSONDecoder()
                    let responseParseHelper = try decoder.decode(AlbumDetailsParseHelperObject.self, from: data)
                    let albumDetailsResponse = AlbumDetailsResponse(parseHelperObject: responseParseHelper)
                    
                    completion(albumDetailsResponse, responseData.error)
                } catch {
                    completion(nil, error)
                }
            }
        }
    }
}
