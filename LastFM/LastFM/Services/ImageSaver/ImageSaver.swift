//
//  ImageSaver.swift
//  LastFM
//
//  Created by Olena Synenka on 2/14/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

final class ImageSaver {
    
    private enum Constants {
        static let imagesDirectory = "SavedImages"
    }
    
    class func fetchImageWithName(_ name: String) -> UIImage? {
        var image: UIImage? = nil
        if let destinationDirectoryURL = directoryPath(for: Constants.imagesDirectory) {
            let imageName = name + ".png"
            let imageUrl = destinationDirectoryURL.appendingPathComponent(imageName)
            if FileManager.default.fileExists(atPath: imageUrl.path) {
                do {
                    let data = try Data(contentsOf: imageUrl)
                    image = UIImage(data: data)
                } catch {
                    print("Image cannot be fetched. " + error.localizedDescription)
                }
                
                return image
            }
        }
        
        return image
    }
    
    class func saveImageWithName(_ image: UIImage, name: String) {
        if let destinationDirectoryURL = directoryPath(for: Constants.imagesDirectory) {
            do {
                let imageName = name + ".png"
                let imageUrl = destinationDirectoryURL.appendingPathComponent(imageName)
                if !FileManager.default.fileExists(atPath: imageUrl.path) {
                    try image.pngData()?.write(to: imageUrl, options: .atomicWrite)
                }
            } catch  (let error) {
                print("Image cannot be saved. " + error.localizedDescription)
            }
        }
    }
    
    class func removeImageWithName(name: String) {
        if let destinationDirectoryURL = directoryPath(for: Constants.imagesDirectory) {
            let imageName = name + ".png"
            let imageUrl = destinationDirectoryURL.appendingPathComponent(imageName)
            if FileManager.default.fileExists(atPath: imageUrl.path) {
                do {
                    try FileManager.default.removeItem(at: imageUrl)
                } catch  (let error) {
                    print("Failed to remove image. " + error.localizedDescription)
                }
            }
        }
    }
    
    //MARK: - Private
    
    private class func directoryPath(for name: String) -> URL? {
        var destinationDirectoryURL: URL?
        if let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            destinationDirectoryURL = URL(fileURLWithPath: documentsDirectory)
            if var directoryURL = destinationDirectoryURL {
                directoryURL.appendPathComponent(name)
                destinationDirectoryURL = directoryURL
                do {
                    try FileManager.default.createDirectory(at: directoryURL, withIntermediateDirectories: true, attributes: nil)
                } catch {
                    print("Cannot fetch path to directory \(name)")
                }
            }
        }
        return destinationDirectoryURL
    }

}
