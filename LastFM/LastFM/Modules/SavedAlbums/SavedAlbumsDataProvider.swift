//
//  SavedAlbumsDataProvider.swift
//  LastFM
//
//  Created by Olena Synenka on 2/9/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

protocol SavedAlbumsDataProviderDelegate {
    
    func dataProviderDidSelectAlbum(_ album: Album)
}

final class SavedAlbumsDataProvider: NSObject {
    
    var delegate: SavedAlbumsDataProviderDelegate?
    
    private var albums: [Album] = []
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    // MARK: - Public
    
    func setup() {
        configureCollectionView()
        albums = fetchSavedAlbums()
    }
    
    func update() {
        albums = fetchSavedAlbums()
        collectionView.reloadData()
    }
    
    func containsAnyAlbums() -> Bool {
        return !albums.isEmpty
    }

    // MARK: - Private
    
    private func fetchSavedAlbums() -> [Album] {
        let albums = StorageService.enquireAlbums()
        return albums
    }
    
    private func configureCollectionView() {
        registerCell()
    }
    
    private func registerCell() {
        let identifier = String(describing: SavedAlbumCollectionViewCell.self)
        let nib = UINib(nibName: identifier, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: identifier)
    }
}

extension SavedAlbumsDataProvider: UICollectionViewDataSource {
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albums.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SavedAlbumCollectionViewCell.self), for: indexPath)
        
        let album = albums[indexPath.item]
        (cell as? SavedAlbumCollectionViewCell)?.configureWithAlbum(album)
        
        return cell
    }
}

extension SavedAlbumsDataProvider: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let album = albums[indexPath.item]
        delegate?.dataProviderDidSelectAlbum(album)
    }
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width / 2
        let height = width * 1.4
        let size = CGSize(width: width, height: height)
        
        return size
    }
}

