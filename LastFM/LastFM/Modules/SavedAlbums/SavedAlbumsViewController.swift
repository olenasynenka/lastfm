//
//  SavedAlbumsViewController.swift
//  LastFM
//
//  Created by Olena Synenka on 2/9/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

final class SavedAlbumsViewController: UIViewController {
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private var dataProvider: SavedAlbumsDataProvider!
    
    private var noSavedAlbumsView: UIView?
    
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataProvider.setup()
        dataProvider.delegate = self
        
        configureNavigationBar()
        configureSearchBarButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dataProvider.update()
        
        if !dataProvider.containsAnyAlbums() {
            showNoSavedAlbumsView()
        } else if noSavedAlbumsView != nil {
            noSavedAlbumsView?.removeFromSuperview()
            noSavedAlbumsView = nil
        }
    }
    
    // MARK: - Private
    
    private func configureNavigationBar() {
        navigationController?.navigationBar.barTintColor = UIColor.lastFMGreen
        title = "Saved Albums"
        
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    private func configureSearchBarButton() {
        let buttonHeight: CGFloat = 24
        
        let image = UIImage(named: "searchIcon")
        let buttonFrame = CGRect(x: 0, y: 0, width: buttonHeight, height: buttonHeight)
        let searchButton = UIButton(frame: buttonFrame)
        searchButton.setImage(image, for: .normal)
        searchButton.tintColor = UIColor.white
        searchButton.addTarget(self, action: #selector(searchButtonPressed), for: .touchUpInside)
        
        let button = UIBarButtonItem(customView: searchButton)
        
        let widthConstraint = button.customView?.widthAnchor.constraint(equalToConstant: buttonHeight)
        widthConstraint?.isActive = true
        let heightConstraint = button.customView?.heightAnchor.constraint(equalToConstant: buttonHeight)
        heightConstraint?.isActive = true
        
        navigationItem.setRightBarButton(button, animated: false)
    }
    
    @objc private func searchButtonPressed() {
        performSegue(withIdentifier: "ShowArtistsSearchSegue", sender: nil)
    }
    
    private func showNoSavedAlbumsView() {
        let noSavedAlbumsView = NoSavedAlbumsView(frame: collectionView.frame)
        view.addSubviewWithConstraints(noSavedAlbumsView)
        view.layoutIfNeeded()
        
        self.noSavedAlbumsView = noSavedAlbumsView
    }
}

extension SavedAlbumsViewController: SavedAlbumsDataProviderDelegate {
    
    // MARK: - SavedAlbumsDataProviderDelegate
    
    func dataProviderDidSelectAlbum(_ album: Album) {
        let albumDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: AlbumDetailsViewController.self)) as? AlbumDetailsViewController
        
        if let albumDetailsViewController = albumDetailsViewController{
            albumDetailsViewController.configureWithAlbum(album)
            
            navigationController?.pushViewController(albumDetailsViewController, animated: true)
        }
    }
}
