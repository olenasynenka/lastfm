//
//  SavedAlbumCollectionViewCell.swift
//  LastFM
//
//  Created by Olena Synenka on 2/9/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

final class SavedAlbumCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var albumCoverImageView: UIImageView!
    @IBOutlet private weak var albumTitleLabel: UILabel!
    @IBOutlet private weak var artistLabel: UILabel!
    @IBOutlet private weak var containerView: UIView!
    
    // MARK: - LifeCycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        albumCoverImageView.image = nil
        albumTitleLabel.text = nil
        artistLabel.text = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupInitialUIAppearence()
    }

    
    // MARK: - Public
    
    func configureWithAlbum(_ album: Album) {
        albumTitleLabel.text = album.name
        artistLabel.text = album.artist?.name
        albumCoverImageView.image = album.mediumImage
        configureViews()
    }
    
    // MARK: - Private
    
    private func setupInitialUIAppearence() {
        albumCoverImageView.tintColor = UIColor.lastFMGrey
        
        containerView.layer.cornerRadius = 4
        containerView.layer.masksToBounds = true
    }
    
    private func configureViews() {
        if albumCoverImageView.image == nil {
            let albumCoverPlaceholderImage = UIImage(named: "albumCoverPlaceholer")
            albumCoverImageView.image = albumCoverPlaceholderImage
        }
    }
}
