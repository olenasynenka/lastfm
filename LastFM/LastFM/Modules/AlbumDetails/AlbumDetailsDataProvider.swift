//
//  AlbumDetailsDataProvider.swift
//  LastFM
//
//  Created by Olena Synenka on 2/12/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

protocol AlbumDetailsDataProviderDelegate {
    
    func dataProviderDidDownloadCoverImage(_ image: UIImage)
    func dataProviderDidAskToShowError(_ error: Error)
}

final class AlbumDetailsDataProvider: NSObject {
    
    private enum Constants {
        static let trackCellsOffset = 1
    }
    
    var delegate: AlbumDetailsDataProviderDelegate?
    
    @IBOutlet private weak var tracksTableView: UITableView!
    
    private var album: Album?
    
    // MARK: - Public
    
    func setup() {
        configureTableView()
    }
    
    func configureWithAlbum(_ album: Album) {
        self.album = album
        
        if album.tracks?.isEmpty ?? true {
            fetchAlbumsDetails()
            fetchAlbumCoverImage { [weak self] (image, error) in
                if let image = image {
                    self?.delegate?.dataProviderDidDownloadCoverImage(image)
                } else if let error = error {
                    self?.delegate?.dataProviderDidAskToShowError(error)
                }
            }
        }
    }
    
    func getaAlbumCover() -> UIImage? {
        return album?.largeImage
    }
    
    // MARK: - Private
    
    private func fetchAlbumCoverImage(completion: @escaping ((UIImage?, Error?) -> ())) {
        guard let imageURL = album?.largeImageURL else {
            //pass error here
            completion(nil, nil)
            return
        }
        
        APIService.shared.fetchImageFor(urlString: imageURL) { [weak self] (image, error) in
            if let image = image,
                let album = self?.album {
                self?.album?.addLargeImage(image)
                
                if self?.album?.isLiked ?? false {
                    StorageService.saveAlbum(album)
                }
            }
            
            completion(image, error)
        }
    }
    
    private func configureTableView() {
        var identifier = String(describing: AlbumTrackTableViewCell.self)
        var nib = UINib(nibName: identifier, bundle: nil)
        tracksTableView.register(nib, forCellReuseIdentifier: identifier)
        
        identifier = String(describing: AlbumDetailsTableViewCell.self)
        nib = UINib(nibName: identifier, bundle: nil)
        tracksTableView.register(nib, forCellReuseIdentifier: identifier)
        
        tracksTableView.allowsSelection = false
        tracksTableView.estimatedRowHeight = UITableView.automaticDimension
    }
    
    private func fetchAlbumsDetails() {
        guard let album = album else {
            return
        }
        
        APIService.shared.fetchAlbumDetails(album.id) { [weak self] (response, error) in
            if error == nil,
                let response = response {
                var tracks: [Track] = []
                let trackResponses = response.tracks
                trackResponses.forEach({
                    let track = Track(trackResponse: $0)
                    tracks.append(track)
                })
                
                self?.album?.setupWithTracks(tracks)
                
                if self?.album?.isLiked ?? false {
                    StorageService.saveAlbum(album)
                }
                
                self?.tracksTableView.reloadData()
            } else if let error = error {
                self?.delegate?.dataProviderDidAskToShowError(error)
            }
        }
    }
}

extension AlbumDetailsDataProvider: UITableViewDataSource {
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let tracksCount = album?.tracks?.count ?? 0
        let number = tracksCount + Constants.trackCellsOffset
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AlbumTrackTableViewCell.self), for: indexPath)
        
        if indexPath.row == 0,
            let album = album {
            cell = tableView.dequeueReusableCell(withIdentifier: String(describing: AlbumDetailsTableViewCell.self), for: indexPath)
            let albumDetailsCell = cell as? AlbumDetailsTableViewCell
            albumDetailsCell?.configureWithAlbum(album)
            albumDetailsCell?.delegate = self
        } else {
            let trackIndex = indexPath.row - Constants.trackCellsOffset
            if let track = album?.tracks?[trackIndex] {
                (cell as? AlbumTrackTableViewCell)?.configureWithTrack(track)
            }
        }
        
        return cell
    }
}

extension AlbumDetailsDataProvider: AlbumDetailsTableViewCellDelegate {
    
    // MARK: - AlbumDetailsTableViewCellDelegate
    
    func albumDetailsCellDidPressLikeButton(cell: AlbumDetailsTableViewCell) {
        guard let album = album else {
            return
        }
        
        if album.isLiked {
            album.dislike()
            StorageService.removeAlbum(album)
        } else {
            album.like()
            StorageService.saveAlbum(album)
        }
        
        cell.updateLikeButton(album.isLiked)
    }
}
