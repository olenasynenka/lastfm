//
//  AlbumDetailsViewController.swift
//  LastFM
//
//  Created by Olena Synenka on 2/12/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

final class AlbumDetailsViewController: UIViewController {
    
    private enum Constants {
        static let albumCoverImageViewHeight: CGFloat = 380
        static let albumCoverImageViewCutHeight: CGFloat = 80
    }
    
    @IBOutlet private weak var tracksTableView: UITableView!
    @IBOutlet private var dataProvider: AlbumDetailsDataProvider!
    
    private var albumCoverImageView: UIImageView = UIImageView()
    private var albumCoverViewMaskLayer: CAShapeLayer = CAShapeLayer()
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataProvider.setup()
        configureViews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        updateAlbumCoverImageView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.hidesBarsOnSwipe = false
    }
    
    // MARK: - Public
    
    func configureWithAlbum(_ album: Album) {
        dataProvider.delegate = self
        dataProvider.configureWithAlbum(album)
        title = album.name
    }
    
    // MARK: - Private
    
    private func configureViews() {
        configureBackButton()
        
        createAlbumCoverImageView()
        
        navigationController?.hidesBarsOnSwipe = true
    }
    
    private func createAlbumCoverImageView() {
        var albumCover = dataProvider.getaAlbumCover()
        if albumCover == nil {
            albumCover = UIImage(named: "largeAlbumCoverPlaceHolder")
        }
        albumCoverImageView.image = albumCover
        
        albumCoverImageView.contentMode = .scaleAspectFill
        albumCoverImageView.layer.masksToBounds = true
        tracksTableView.addSubview(albumCoverImageView)
        
        let effectiveHeight = Constants.albumCoverImageViewHeight - Constants.albumCoverImageViewCutHeight / 2
        tracksTableView.contentInset = UIEdgeInsets(top: effectiveHeight, left: 0, bottom: 0, right: 0)
        tracksTableView.contentOffset = CGPoint(x: 0, y: -effectiveHeight)
        
        createAlbumCoverMaskLayer()
        
        updateAlbumCoverImageView()
    }
    
    private func createAlbumCoverMaskLayer() {
        albumCoverViewMaskLayer = CAShapeLayer()
        albumCoverViewMaskLayer.fillColor = UIColor.black.cgColor
        
        albumCoverImageView.layer.mask = albumCoverViewMaskLayer
    }
    
    private func updateAlbumCoverImageView() {
        let effectiveHeight = Constants.albumCoverImageViewHeight - Constants.albumCoverImageViewCutHeight / 2
        var headerRect = CGRect(x: 0, y: -effectiveHeight, width: tracksTableView.bounds.width, height: Constants.albumCoverImageViewHeight)
        if tracksTableView.contentOffset.y < -effectiveHeight {
            headerRect.origin.y = tracksTableView.contentOffset.y
            headerRect.size.height = -tracksTableView.contentOffset.y + Constants.albumCoverImageViewCutHeight / 2
        }
        albumCoverImageView.frame = headerRect
        
        updateAlbumCoverMaskLayerPath(headerRect)
    }
    
    private func updateAlbumCoverMaskLayerPath(_ headerFrame: CGRect) {
        let path = UIBezierPath()
        path.move(to: .zero)
        path.addLine(to: CGPoint(x: headerFrame.width, y: 0))
        path.addLine(to: CGPoint(x: headerFrame.width, y: headerFrame.height - Constants.albumCoverImageViewCutHeight))
        path.addLine(to: CGPoint(x: 0, y: headerFrame.height))
        
        albumCoverViewMaskLayer.path = path.cgPath
    }
}

extension AlbumDetailsViewController: UIScrollViewDelegate {
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateAlbumCoverImageView()
    }
}

extension AlbumDetailsViewController: UITableViewDelegate {
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension AlbumDetailsViewController: AlbumDetailsDataProviderDelegate {
    
    // MARK: - AlbumDetailsDataProviderDelegate
    
    func dataProviderDidDownloadCoverImage(_ image: UIImage) {
        UIView.transition(with: albumCoverImageView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.albumCoverImageView.image = image
        }, completion: { _ in
            self.updateAlbumCoverImageView()
        })
    }
    
    func dataProviderDidAskToShowError(_ error: Error) {
        showErrorMessage(error.localizedDescription)
    }
}
