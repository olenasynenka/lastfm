//
//  AlbumTrackTableViewCell.swift
//  LastFM
//
//  Created by Olena Synenka on 2/12/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

class AlbumTrackTableViewCell: UITableViewCell {

    @IBOutlet private weak var numberLabel: UILabel!
    @IBOutlet private weak var trackNameLabel: UILabel!
    @IBOutlet private weak var durationLabel: UILabel!
    
    // MARK: - Public
    
    func configureWithTrack(_ track: Track) {
        numberLabel.text = "\(track.number)"
        trackNameLabel.text = track.name
        
        let durationString = convertDurationToReadableFormat(track.duration)
        durationLabel.text = durationString
    }
    
    // MARK: - Private
    
    private func convertDurationToReadableFormat(_ duration: Int) -> String? {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute, .second]
        formatter.unitsStyle = .positional
        
        let formattedString = formatter.string(from: TimeInterval(duration))
        
        return formattedString
    }
}
