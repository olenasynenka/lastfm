//
//  AlbumDetailsTableViewCell.swift
//  LastFM
//
//  Created by Olena Synenka on 2/13/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

protocol AlbumDetailsTableViewCellDelegate {
    
    func albumDetailsCellDidPressLikeButton(cell: AlbumDetailsTableViewCell)
}

class AlbumDetailsTableViewCell: UITableViewCell {
    
    var delegate: AlbumDetailsTableViewCellDelegate?

    @IBOutlet private weak var likeButton: UIButton!
    @IBOutlet private weak var albumNameLabel: UILabel!
    @IBOutlet weak var artistPhotoImageView: UIImageView!
    @IBOutlet private weak var artistNameLabel: UILabel!
    @IBOutlet private weak var tracksCountLabel: UILabel!
    
    // MARK: - Actions
    
    @IBAction func likeButtonPressed(_ sender: Any) {
        delegate?.albumDetailsCellDidPressLikeButton(cell: self)
    }
    
    // MARK: - Public
    
    func updateLikeButton(_ isLiked: Bool) {
        if isLiked {
            scaleHeartAnimation()
        }
        
        let imageName = isLiked ? "filledHeartIcon" : "emptyHeartIcon"
        let image = UIImage(named: imageName)
        likeButton.setImage(image, for: .normal)
    }

    func configureWithAlbum(_ album: Album) {
        let imageName = album.isLiked ? "filledHeartIcon" : "emptyHeartIcon"
        let image = UIImage(named: imageName)
        likeButton.setImage(image, for: .normal)
        
        albumNameLabel.text = album.name
        artistPhotoImageView.image = album.artist?.image
        artistNameLabel.text = album.artist?.name
        tracksCountLabel.text = "\(album.tracks?.count ?? 0) song(s)"
    }
    
    // MARK: - Private
    
    private func scaleHeartAnimation() {
        let scaleAnimation: CAKeyframeAnimation = CAKeyframeAnimation(keyPath: "transform")
        scaleAnimation.values = [
            NSValue(caTransform3D: CATransform3DIdentity),
            NSValue(caTransform3D: CATransform3DMakeScale(1.3, 1.3, 1.3)),
            NSValue(caTransform3D: CATransform3DIdentity)
        ]
        
        scaleAnimation.autoreverses = true
        scaleAnimation.duration = 0.2
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        
        likeButton.layer.add(scaleAnimation, forKey: "scale")
    }
}
