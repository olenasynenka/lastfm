//
//  SearchTableViewCell.swift
//  LastFM
//
//  Created by Olena Synenka on 2/10/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

final class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var artistPhotoImageView: UIImageView!
    @IBOutlet weak var artistNameLabel: UILabel!
    
    private var activityIndicator: UIActivityIndicatorView?
    
    // MARK: - LifeCycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        artistPhotoImageView.image = nil
        artistNameLabel.text = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureViews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        artistPhotoImageView.layer.cornerRadius = artistPhotoImageView.frame.height / 2
    }
    
    // MARK: - Public
    
    func configureWith(artist: Artist) {
        self.artistNameLabel.text = artist.name
        showImageLoader()
    }
    
    func setArtistImage(_ image: UIImage) {
        activityIndicator?.stopAnimating()
        
        artistPhotoImageView.image = image
        
        activityIndicator?.removeFromSuperview()
        activityIndicator = nil
    }
    
    // MARK: - Private
    
    private func showImageLoader() {
        artistPhotoImageView?.backgroundColor = UIColor.lastFMGrey
        let activityIndicator = UIActivityIndicatorView(style: .white)
        artistPhotoImageView?.addSubview(activityIndicator)
        
//        let heightConstraint = NSLayoutConstraint(item: activityIndicator, attribute: .height, relatedBy: .equal, toItem: artistPhotoImageView, attribute: .height, multiplier: 1, constant: 0)
//        let widthConstraint = NSLayoutConstraint(item: activityIndicator, attribute: .width, relatedBy: .equal, toItem: artistPhotoImageView, attribute: .width, multiplier: 1, constant: 0)
//        heightConstraint.isActive = true
//        widthConstraint.isActive = true
//        
//        layoutIfNeeded()
        
        activityIndicator.startAnimating()
        
        self.activityIndicator = activityIndicator
    }
    
    private func configureViews() {
        artistPhotoImageView.layer.masksToBounds = true
    }
}
