//
//  ArtistsSearchViewController.swift
//  LastFM
//
//  Created by Olena Synenka on 2/10/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

final class ArtistsSearchViewController: UIViewController {
    
    private let searchController: UISearchController = UISearchController(searchResultsController: nil)
    @IBOutlet private var dataProvider: ArtistsSearchDataProvider!
    @IBOutlet private weak var artistsTableView: UITableView!
    
    private var activityIndicatorView: UIActivityIndicatorView?
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureSearchController()
        
        dataProvider.setup()
        dataProvider.delegate = self
        
        configureBackButton()
        
        title = "Search Artists"
    }
    
    // MARK: - Private
    
    private func configureSearchController () {
        searchController.searchResultsUpdater = self
        searchController.delegate = self

        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Artists"
        searchController.searchBar.tintColor = UIColor.white
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        navigationItem.hidesSearchBarWhenScrolling = false
    }
}

extension ArtistsSearchViewController: UISearchResultsUpdating, UISearchControllerDelegate {
    
    // MARK: - UISearchResultsUpdating Delegate
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        if let searchQuery = searchBar.text {
            dataProvider.searchArtistsForInputQuery(searchQuery)
        }
    }
}

extension ArtistsSearchViewController: ArtistsSearchDataProviderDelegate {
    
    // MARK: - ArtistsSearchDataProviderDelegate

    func dataProviderDidSelectArtist(_ artist: Artist) {
        let artistAlbumsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: ArtistAlbumsViewController.self)) as? ArtistAlbumsViewController
        
        if let artistAlbumsViewController = artistAlbumsViewController {
            artistAlbumsViewController.configureWithArtist(artist)
            
            navigationController?.pushViewController(artistAlbumsViewController, animated: true)
        }
    }
    
    func dataProviderDidAskToShowLoader() {
        if activityIndicatorView == nil {
            activityIndicatorView = UIActivityIndicatorView(style: .gray)
        }

        artistsTableView.backgroundView = activityIndicatorView
        activityIndicatorView?.startAnimating()
    }
    
    func dataProviderDidAskToHideLoader() {
        activityIndicatorView?.stopAnimating()
        artistsTableView.backgroundView = nil
    }
    
    func dataProviderDidAskToHandleError(_ error: Error) {
        dataProviderDidAskToHideLoader()
        
        showErrorMessage(error.localizedDescription)
    }
}
