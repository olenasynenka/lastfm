//
//  ArtistsSearchDataProvider.swift
//  LastFM
//
//  Created by Olena Synenka on 2/10/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

protocol ArtistsSearchDataProviderDelegate: class {
    
    func dataProviderDidSelectArtist(_ artist: Artist)
    func dataProviderDidAskToHideLoader()
    func dataProviderDidAskToShowLoader()
    
    func dataProviderDidAskToHandleError(_ error: Error)
}

final class ArtistsSearchDataProvider: NSObject {
    
    weak var delegate: ArtistsSearchDataProviderDelegate?
    
    @IBOutlet private weak var tableView: UITableView!
    
    private var artists: [Artist]?
    private var request: (() -> ())?
    private var timer: Timer?
    private var currentQuery: String?
    
    private var currentPage: Int = 1
    private var totalResultsCount: Int = 0
    
    // MARK: - Public
    
    func setup() {
        configureTableView()
    }
    
    func searchArtistsForInputQuery(_ inputQuery: String) {
        guard !inputQuery.isEmpty else {
            return
        }
        
        if artists?.isEmpty ?? true {
            delegate?.dataProviderDidAskToShowLoader()
        }
        
        currentQuery = inputQuery
        currentPage = 1

        fetchArtistsFor(query: inputQuery, page: currentPage, completion: { [weak self] response in
            if let response = response {
                
                let previousArtistCount = self?.artists?.count ?? 0
                self?.artists = []
                response.artistResponses.forEach({
                    let artist = Artist(name: $0.name,
                                        id: $0.id,
                                        imageURL: $0.imageURL)
                    self?.artists?.append(artist)
                })
                
                self?.totalResultsCount = response.totalResults
                self?.delegate?.dataProviderDidAskToHideLoader()
                
                var indexPathsToRemove: [IndexPath] = []
                for index in 0..<previousArtistCount {
                    let indexPath = IndexPath(row: index, section: 0)
                    indexPathsToRemove.append(indexPath)
                }
                
                var indexPathsToInsert: [IndexPath] = []
                for index in 0..<(self?.artists?.count ?? 0) {
                    let indexPath = IndexPath(row: index, section: 0)
                    indexPathsToInsert.append(indexPath)
                }
                
                self?.tableView.beginUpdates()
                    self?.tableView.deleteRows(at: indexPathsToRemove, with: .fade)
                    self?.tableView.insertRows(at: indexPathsToInsert, with: .fade)
                self?.tableView.endUpdates()
            }
        })
    }
    
    // MARK: - Private
    
    private func configureTableView() {
        registerCell()
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
    }
    
    private func registerCell() {
        let identifier = String(describing: SearchTableViewCell.self)
        let nib = UINib(nibName: identifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: identifier)
    }
    
    private func fetchArtistsFor(query: String, page: Int, completion: @escaping ((SearchArtistsResponse?) -> ())) {
        createSearchArtistsRequest(query, page: page, completion: { [weak self] (response, error) in
            if error == nil {
                completion(response)
            } else if let error = error {
                self?.delegate?.dataProviderDidAskToHandleError(error)
                
                completion(nil)
            }
        })
    }
    
    private func createSearchArtistsRequest(_ inputQuery: String, page: Int, completion: @escaping ((SearchArtistsResponse?, Error?) -> ())) {
        if request != nil {
            timer?.invalidate()
        }
        
        request = {
            APIService.shared.fetchArtistForSearchQuery(inputQuery, page: page, completion: completion)
        }
        
        timer = Timer(timeInterval: 0.1, repeats: false) { _ in
            self.request?()
        }
        
        if let timer = timer {
            RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
        }
    }
    
    private func loadImageFor(artist: Artist, indexPath: IndexPath) {
        APIService.shared.fetchImageFor(urlString: artist.imageURL) { [weak self] (image, error) in
            if error == nil,
                let image = image {
                DispatchQueue.main.async {
                    artist.setArtistPhotoImage(image)
            
                    if let affectedCell = self?.tableView.cellForRow(at: indexPath) as? SearchTableViewCell {
                        affectedCell.setArtistImage(image)
                    }
                }
            }
        }
    }
    
    private func fetchNextPage() {
        guard let currentQuery = currentQuery else {
            return
        }
        
        self.fetchArtistsFor(query: currentQuery, page: currentPage + 1, completion: { [weak self] (response) in
            if let strongSelf = self,
                let response = response {
                strongSelf.currentPage += 1
                
                let startIndex = strongSelf.artists?.count ?? 0
                response.artistResponses.forEach({
                    let artist = Artist(name: $0.name,
                                        id: $0.id,
                                        imageURL: $0.imageURL)
                    self?.artists?.append(artist)
                })
                
                let aristsCount = strongSelf.artists?.count ?? 0
                var indexPathsToInsert: [IndexPath] = []
                for index in startIndex..<aristsCount {
                    let indexPath = IndexPath(row: index, section: 0)
                    indexPathsToInsert.append(indexPath)
                }
                
                strongSelf.tableView.beginUpdates()
                    strongSelf.tableView.insertRows(at: indexPathsToInsert, with: .automatic)
                strongSelf.tableView.endUpdates()
            }
        })
    }
}

extension ArtistsSearchDataProvider: UITableViewDataSource {
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let fetchArtistsCount = artists?.count ?? 0
        return fetchArtistsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SearchTableViewCell.self), for: indexPath)
        
        if let artist = artists?[indexPath.row],
            let cell = cell as? SearchTableViewCell {
            cell.configureWith(artist: artist)
            loadImageFor(artist: artist, indexPath: indexPath)
        }
        
        if let fetchArtistsCount = artists?.count,
            indexPath.row == fetchArtistsCount - 10,
            fetchArtistsCount < totalResultsCount {
            fetchNextPage()
        }
        
        return cell
    }
}

extension ArtistsSearchDataProvider: UITableViewDelegate {
    
    // MARK: - UITableViewDelegate

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let artist = artists?[indexPath.row] {
            delegate?.dataProviderDidSelectArtist(artist)
        }
    }
}
