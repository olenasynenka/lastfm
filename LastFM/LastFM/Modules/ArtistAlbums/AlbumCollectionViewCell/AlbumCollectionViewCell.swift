//
//  AlbumCollectionViewCell.swift
//  LastFM
//
//  Created by Olena Synenka on 2/12/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

protocol AlbumCollectionViewCellDelegate {
    
    func albumDetailsCellDidPressLikeButton(cell: AlbumCollectionViewCell)
}

final class AlbumCollectionViewCell: UICollectionViewCell {
    
    var delegate: AlbumCollectionViewCellDelegate?

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var detailsContainerView: UIView!
    @IBOutlet weak var albumCoverImageView: UIImageView!
    @IBOutlet private weak var albumTitleLabel: UILabel!
    @IBOutlet private weak var likeButton: UIButton!
    
    // MARK: - LifeCycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupInitialUIAppearence()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        albumCoverImageView.image = nil
        albumTitleLabel.text = nil
    }
    
    // MARK: - Actions
    
    @IBAction func likeButtonPressed(_ sender: Any) {
        delegate?.albumDetailsCellDidPressLikeButton(cell: self)
    }
    
    // MARK: - Public
    
    func updateLikeButton(_ isLiked: Bool) {
        if isLiked {
            scaleHeartAnimation()
        }
        
        let imageName = isLiked ? "filledHeartIcon" : "emptyHeartIcon"
        let image = UIImage(named: imageName)
        likeButton.setImage(image, for: .normal)
    }
    
    func configureWithAlbum(_ album: Album) {
        let imageName = album.isLiked ? "filledHeartIcon" : "emptyHeartIcon"
        let image = UIImage(named: imageName)
        likeButton.setImage(image, for: .normal)
        
        albumTitleLabel.text = album.name
        configureViews()
    }
    
    // MARK: - Private
    
    private func setupInitialUIAppearence() {
        albumCoverImageView.tintColor = UIColor.lastFMGrey
        
        containerView.layer.cornerRadius = 4
        containerView.layer.masksToBounds = true
    }
    
    private func configureViews() {
        let albumCoverPlaceholderImage = UIImage(named: "albumCoverPlaceholer")
        albumCoverImageView.image = albumCoverPlaceholderImage
    }
        
    private func scaleHeartAnimation() {
        let scaleAnimation: CAKeyframeAnimation = CAKeyframeAnimation(keyPath: "transform")
        scaleAnimation.values = [
            NSValue(caTransform3D: CATransform3DIdentity),
            NSValue(caTransform3D: CATransform3DMakeScale(1.3, 1.3, 1.3)),
            NSValue(caTransform3D: CATransform3DIdentity)
        ]
        
        scaleAnimation.autoreverses = true
        scaleAnimation.duration = 0.2
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        
        likeButton.layer.add(scaleAnimation, forKey: "scale")
    }
}
