//
//  ArtistAlbumsViewController.swift
//  LastFM
//
//  Created by Olena Synenka on 2/11/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

final class ArtistAlbumsViewController: UIViewController {
    
    @IBOutlet private final weak var albumsCollectionView: UICollectionView!
    @IBOutlet private var dataProvider: ArtistAlbumsDataProvider!
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataProvider.setup()
        dataProvider.delegate = self
        configureViews()
    }
    
    // MARK: - Public
    
    func configureWithArtist(_ artist: Artist) {
        dataProvider.configureWithArtist(artist)
        title = artist.name
    }
    
    // MARK: - Private
    
    private func configureViews() {
        configureBackButton()
        albumsCollectionView.backgroundColor = UIColor.lastFMGrey
    }
}

extension ArtistAlbumsViewController: ArtistAlbumsDataProviderDelegate {
    
    // MARK: - ArtistAlbumsDataProviderDelegate
    
    func dataProviderDidSelectAlbum(_ album: Album) {
        let albumDetailsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: AlbumDetailsViewController.self)) as? AlbumDetailsViewController
        
        if let albumDetailsViewController = albumDetailsViewController{
            albumDetailsViewController.configureWithAlbum(album)
            
            navigationController?.pushViewController(albumDetailsViewController, animated: true)
        }
    }
    
    func dataProviderDidAskToShowErrorMessage(_ message: String) {
        showErrorMessage(message)
    }
}
