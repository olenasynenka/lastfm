//
//  ArtistAlbumsDataProvider.swift
//  LastFM
//
//  Created by Olena Synenka on 2/11/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

protocol ArtistAlbumsDataProviderDelegate {
    
    func dataProviderDidSelectAlbum(_ album: Album)
    func dataProviderDidAskToShowErrorMessage(_ message: String)
}

final class ArtistAlbumsDataProvider: NSObject {
    
    var delegate: ArtistAlbumsDataProviderDelegate?
    
    @IBOutlet private weak var albumsCollectionView: UICollectionView!
    private var albums: [Album] = []
    private var savedAlbums: [Album] = []
    private var artist: Artist?
    
    private var currentPage: Int = 1
    private var totalAlbumsCount: Int = 0
    
    // MARK: - Public
    
    func configureWithArtist(_ artist: Artist) {
        self.artist = artist
        fetchSavedAlbumsForArtist()
        fetchTopAlbums()
    }
    
    func setup() {
        configureCollectionView()
    }
    
    func updateLayout() {
        albumsCollectionView.collectionViewLayout.invalidateLayout()
        albumsCollectionView.layoutIfNeeded()
    }
        
    // MARK: - Private
    
    private func configureCollectionView() {
        let identifier = String(describing: AlbumCollectionViewCell.self)
        let nib = UINib(nibName: identifier, bundle: nil)
        albumsCollectionView.register(nib, forCellWithReuseIdentifier: identifier)
    }
    
    // MARK: DataFetching
    
    private func fetchTopAlbums() {
        fetchAlbumsForArtist(page: currentPage, completion: { [weak self]  albums in
            if let albums = albums {
                let startIndex = self?.albums.count ?? 0
                self?.albums.append(contentsOf: albums)
                
                self?.insertAlbumsToCollectionView(startIndex, endIndex: self?.albums.count ?? 0)
            }
        })
    }
    
    private func fetchAlbumsForArtist(page: Int, completion: @escaping (([Album]?) -> ())) {
        guard let artistId = artist?.id else {
            return
        }
        
        APIService.shared.fetchAlbumsForArtist(artistId, page: page) { [weak self] (response, error) in
            if error == nil,
                let artist = self?.artist {
                self?.totalAlbumsCount = response?.totalResults ?? 0
                
                let unsavedAlbums = response?.albums.filter({ album in
                    return !(self?.savedAlbums.contains(where: { $0.id == album.id }) ?? false)
                })
                
                var albums: [Album] = []
                unsavedAlbums?.forEach({ (albumResponse) in
                    let album = Album(albumResponse: albumResponse, artist: artist)
                    albums.append(album)
                })
                
                completion(albums)
            } else if let error = error {
                self?.delegate?.dataProviderDidAskToShowErrorMessage(error.localizedDescription)
                completion(nil)
            }
        }
    }
    
    private func fetchNextPage() {
        fetchAlbumsForArtist(page: currentPage + 1, completion: { [weak self]  albums in
            if let albums = albums {
                self?.currentPage += 1
                let startIndex = self?.albums.count ?? 0
                self?.albums.append(contentsOf: albums)
                
                self?.insertAlbumsToCollectionView(startIndex, endIndex: self?.albums.count ?? 0)
            }
        })
    }
    
    private func loadImageForAlbum(_ album: Album, at indexPath: IndexPath) {
        APIService.shared.fetchImageFor(urlString: album.mediumImageURL) { [weak self] (image, error) in
            DispatchQueue.main.async {
                if  let image = image,
                    let cell = self?.albumsCollectionView.cellForItem(at: indexPath) as? AlbumCollectionViewCell {
                    album.addMediumImage(image)
                    cell.albumCoverImageView.image = image
                }
            }
        }
    }
    
    private func fetchSavedAlbumsForArtist() {
        guard let artistId = artist?.id else {
            return
        }
        
        savedAlbums = StorageService.enquireAlbumsForArtist(artistId)
        
        albums.append(contentsOf: savedAlbums)
    }
    
    private func insertAlbumsToCollectionView(_ startIndex: Int, endIndex: Int) {
        var indexPathsToInsert: [IndexPath] = []
        for index in startIndex..<endIndex {
            let indexPath = IndexPath(item: index, section: 0)
            indexPathsToInsert.append(indexPath)
        }
        
        albumsCollectionView.performBatchUpdates({
            albumsCollectionView.insertItems(at: indexPathsToInsert)
        }, completion: nil)
    }
}

extension ArtistAlbumsDataProvider: UICollectionViewDataSource {
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albums.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AlbumCollectionViewCell.self), for: indexPath)
        
        let album = albums[indexPath.row]
        if let albumCell = cell as? AlbumCollectionViewCell{
            albumCell.configureWithAlbum(album)
            albumCell.delegate = self
        }
        
        if indexPath.item == albums.count - 10,
            albums.count < totalAlbumsCount {
            fetchNextPage()
        }
        
        return cell
    }
}

extension ArtistAlbumsDataProvider: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let album = albums[indexPath.item]
        loadImageForAlbum(album, at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let album = albums[indexPath.item]
        delegate?.dataProviderDidSelectAlbum(album)
    }
    
    // MARK: - UICollectionViewDelegate

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width / 2
        let height = width * 1.3
        let size = CGSize(width: width, height: height)
        
        return size
    }
}

extension ArtistAlbumsDataProvider: AlbumCollectionViewCellDelegate {
    
    // MARK: - AlbumCollectionViewCellDelegate
    
    func albumDetailsCellDidPressLikeButton(cell: AlbumCollectionViewCell) {
        guard let indexPath = albumsCollectionView.indexPath(for: cell) else {
            return
        }
        
        let album = albums[indexPath.item]
        
        if album.isLiked {
            album.dislike()
            StorageService.removeAlbum(album)
        } else {
            album.like()
            StorageService.saveAlbum(album)
        }
        
        cell.updateLikeButton(album.isLiked)
    }
}
