//
//  UIViewController+BackButton.swift
//  LastFM
//
//  Created by Olena Synenka on 2/10/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func configureBackButton() {
        if let backImage = UIImage(named: "backIcon")?.withRenderingMode(.alwaysTemplate) {
            let backButton = UIBarButtonItem(image: backImage, style: .done, target: self, action: #selector(backAction))
            backButton.tintColor = UIColor.white
            backButton.imageInsets = UIEdgeInsets(top: 0, left: -8, bottom: 0, right: 0)
            navigationItem.hidesBackButton = true
            navigationItem.leftBarButtonItem = backButton
        }
    }
    
    @objc func backAction() {
        navigationController?.popViewController(animated: true)
    }
}
