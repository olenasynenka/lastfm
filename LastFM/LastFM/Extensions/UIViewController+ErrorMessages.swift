//
//  UIViewController+ErrorMessages.swift
//  LastFM
//
//  Created by Olena Synenka on 2/14/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit
import SwiftMessages

extension UIViewController {
    
    func showErrorMessage(_ message: String) {
        let view = MessageView.viewFromNib(layout: .messageView)
        view.configureTheme(.error)
        view.configureDropShadow()
        view.configureContent(body: message)
        view.button?.isHidden = true
        view.titleLabel?.isHidden = true
        
        SwiftMessages.show(view: view)
    }
}
