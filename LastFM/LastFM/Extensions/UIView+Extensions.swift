//
//  UIView+Extensions.swift
//  LastFM
//
//  Created by Olena Synenka on 2/14/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

public extension UIView {
    
    // MARK: - UIView+Extensions
    
    func addSubviewWithConstraints(_ subview: UIView) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        let views = [
            "subview": subview
        ]
        addSubview(subview)
        
        var constraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[subview]|",
                                                         options: [.alignAllLeading, .alignAllTrailing], metrics: nil, views: views)
        constraints.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: "V:|[subview]|",
                                                                      options: [.alignAllTop, .alignAllBottom], metrics: nil, views: views))
        NSLayoutConstraint.activate(constraints)
    }
}
