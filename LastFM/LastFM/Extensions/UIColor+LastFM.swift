//
//  UIColor+LastFM.swift
//  LastFM
//
//  Created by Olena Synenka on 2/10/19.
//  Copyright © 2019 Olena Synenka. All rights reserved.
//

import UIKit

extension UIColor {
    
    class var lastFMGreen: UIColor {
        return UIColor(red: 126.0 / 255.0, green: 211.0 / 255.0, blue: 33.0 / 255.0, alpha: 1.0)
    }
    
    class var lastFMGrey: UIColor {
        return UIColor.black.withAlphaComponent(0.1)
    }
}
